# Script de ejemplo que recorre los archivos de un directorio, lee su contenido, 
# normaliza los tokens que se encuentran dentro, y al final genera un diccionario
# de frecuencias que sera escrito en un archivo de salida
import os
import sys
import re

# Pasa a minusculas y elimina acentos

def normalize(token):
    token = token.replace("_", "")
    token = token.replace("-", "")
    token = sacar_acentos(token)
    return token.lower()

def sacar_acentos(s):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ú", "u"),
    )
    for a, b in replacements:
        s = s.replace(a, b).replace(a.upper(), b.upper())
    return s


def main():
    # Parámetros del programa
    dir_path = sys.argv[1]  # Directorio donde se encuentran los documentos
    eliminar_palabras_vacias = sys.argv[2]  # Si se deben eliminar las palabras vacías
    archivo_palabras_vacias = sys.argv[3]  # Archivo que contiene las palabras vacías, debe tener una palabra por línea
    longitud_minima = 3  # Longitud mínima de los términos a incluir
    longitud_maxima = 50  # Longitud máxima de los términos a incluir

    # Se lee el archivo de palabras vacías si se especifica
    palabras_vacias = set()
    if eliminar_palabras_vacias == "True":
        with open(archivo_palabras_vacias, "r") as f:
            palabras_vacias = set([line.strip() for line in f])

    # Diccionario para almacenar la frecuencia de los términos y el DF
    terminos = {}

    # Se recorre todos los documentos en el directorio
    for filename in os.listdir(dir_path):
        # Ignoramos los archivos ocultos
        if filename.startswith('.'):
            continue

        # Se lee el contenido del archivo
        with open(os.path.join(dir_path, filename), "r") as f:
            contenido = f.read().lower()  # se normaliza a minusculas al leer

        # Se tokeniza el contenido
        tokens = re.findall(r"\w+",
                            contenido)  # Se buscan todas las ocurrencias de palabras en el contenido del archivo.

        # Se aplica la normalización a cada token
        tokens = [normalize(token) for token in tokens]

        # Se eliminan las palabras vacías si se especifica
        if eliminar_palabras_vacias == "True":
            tokens = [token for token in tokens if token not in palabras_vacias]

        # Se Filtran los términos por longitud
        tokens = [token for token in tokens if longitud_minima <= len(token) <= longitud_maxima]

        # Se actualizan el diccionario de términos
        for token in tokens:
            if token not in terminos:
                terminos[token] = [1, {filename}]
            else:
                terminos[token][0] += 1
                terminos[token][1].add(filename)
                # La utilización de un conjunto para almacenar los nombres de archivo tiene sentido porque no queremos
                # contar varias veces el mismo término y es útil para determinar el Document Frequency (DF) de un
                # término

    # Se ordenan los términos por frecuencia
    terminos_ordenados = sorted(terminos.items(), key=lambda x: x[1][0], reverse=True)

    # Se escribe el archivo de términos
    with open("terminos.txt", "w") as f:
        for termino, datos in terminos_ordenados:
            cf = datos[0]
            df = len(datos[1])
            f.write(f"{termino} {cf} {df}\n")


if __name__ == '__main__':
    main()
