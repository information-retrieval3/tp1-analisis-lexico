import os
import re
import sys
from collections import defaultdict, Counter


def main():
    if len(sys.argv) < 2:
        print("Error: No se ha especificado la ruta del directorio de documentos.")
        sys.exit(1)

    dir_path: str = sys.argv[1]
    eliminar_palabras_vacias = False
    palabras_vacias = set()

    if len(sys.argv) > 2 and sys.argv[2] == "True" and len(sys.argv) > 3:
        eliminar_palabras_vacias = True
        archivo_palabras_vacias = sys.argv[3]
        with open(archivo_palabras_vacias, "r", encoding="utf8") as f:
            palabras_vacias = set([line.strip() for line in f])

    # longitud minima y maxima para los terminos
    long_min = 3
    long_max = 30

    # diccionario para almacenar los terminos y sus estadisticas
    terminos = defaultdict(lambda: [0, set()])
    # defaultdict es un tipo de diccionario que automáticamente crea un valor por defecto en caso de que la clave 
    # dada no exista. el primer elemento de la lista es la freceuncia y el segundo es un conjunto con los nombres de 
    # los docs donde aparece

    # def de vaiables para estadisticas 
    documentos_procesados = 0
    tokens_extraidos = 0
    terminos_extraidos = 0
    largo_terminos_total = 0
    tokens_documento_mas_corto = float("inf")
    terminos_documento_mas_corto = float("inf")
    tokens_documento_mas_largo = 0
    terminos_documento_mas_largo = 0
    terminos_con_frecuencia_uno = 0

    for filename in os.listdir(dir_path):
        if filename.endswith(".txt"):  # validamos que sean archivos de tipo texto
            with open(os.path.join(dir_path, filename), "r", encoding="utf8") as f:
                # Leemos el contenido del archivo
                contenido = f.read().lower()  # se normaliza a minusculas al leer
                # Tokenizamos el contenido
                tokens = re.findall(r'\w+', contenido)
                # Eliminamos las palabras vacías si se especifica
                if eliminar_palabras_vacias:
                    tokens = [token for token in tokens if token not in palabras_vacias]
                terminos_documento = defaultdict(lambda: 0)
                for token in tokens:
                    if long_min <= len(token) <= long_max:
                        terminos[token][0] += 1
                        terminos[token][1].add(filename)
                        terminos_documento[token] += 1
                        tokens_extraidos += 1  # punto (2.1)
                terminos_documento_extraidos = len(terminos_documento)
                terminos_extraidos += terminos_documento_extraidos  # punto (2.2)
                largo_terminos_total += sum([len(termino) for termino in terminos_documento])
                if terminos_documento_extraidos > 0:
                    largo_promedio_terminos_documento = \
                        sum([len(termino) for termino in terminos_documento]) / terminos_documento_extraidos
                    if largo_promedio_terminos_documento < terminos_documento_mas_corto:
                        terminos_documento_mas_corto = largo_promedio_terminos_documento  # punto (5.1)
                        tokens_documento_mas_corto = len(tokens)  # punto (5.1)
                    if largo_promedio_terminos_documento > terminos_documento_mas_largo:
                        terminos_documento_mas_largo = largo_promedio_terminos_documento  # punto (5.1)
                        tokens_documento_mas_largo = len(tokens)  # punto (5.2)
                for termino, frecuencia in terminos_documento.items():
                    if frecuencia == 1:
                        terminos_con_frecuencia_uno += 1  # punto (6)
            documentos_procesados += 1  # punto (1)

    # calcular estadisticas generales
    promedio_tokens_documento = tokens_extraidos / documentos_procesados  # punto (3.1)
    promedio_terminos_documento = terminos_extraidos / documentos_procesados  # punto (3.2)
    largo_promedio_termino = largo_terminos_total / terminos_extraidos  # punto (4)

    # Ordenamos los términos por frecuencia
    terminos_ordenados = sorted(terminos.items(), key=lambda x: x[1][0], reverse=True)

    # Escribimos el archivo de términos
    with open("terminos.txt", "w") as f:
        for termino, datos in terminos_ordenados:
            cf = datos[0]
            df = len(datos[1])
            f.write(f"{termino} {cf} {df}\n")

    # Escribimos el archivo de estadisticas
    with open("estadisticas.txt", "w") as f:
        f.write(f"{documentos_procesados}\n")
        f.write(f"{tokens_extraidos} {terminos_extraidos}\n")
        f.write(f"{promedio_tokens_documento:.2f} {promedio_terminos_documento:.2f}\n")
        f.write(f"{largo_promedio_termino:.2f}\n")
        f.write(f"{tokens_documento_mas_corto} {terminos_documento_mas_corto} {tokens_documento_mas_largo} "
                f"{terminos_documento_mas_largo}\n")
        f.write(f"{terminos_con_frecuencia_uno}\n")
    # with open("estadisticas.txt", "w") as f:
    #     f.write(f"Cantidad de documentos procesados: {documentos_procesados}\n")
    #     f.write(f"Cantidad de tokens extraídos: {tokens_extraidos}\n")
    #     f.write(f"Cantidad de términos extraídos: {terminos_documento_extraidos}\n")
    #     f.write(f"Promedio de tokens por documento: {promedio_tokens_documento:.2f}\n")
    #     f.write(f"Promedio de términos por documento: {promedio_terminos_documento:.2f}\n")
    #     f.write(f"Largo promedio de un término: {largo_promedio_termino:.2f}\n")
    #     f.write(f"Cantidad de tokens del documento más corto: {tokens_documento_mas_corto}\n")
    #     f.write(f"Cantidad de tokens del documento más largo: {tokens_documento_mas_largo}\n")
    #     f.write(f"Cantidad de términos que aparecen una vez: {terminos_con_frecuencia_uno}\n")


if __name__ == '__main__':
    main()
