import os
import re
import sys
from collections import defaultdict


def main():
    if len(sys.argv) < 2:
        print("Error: No se ha especificado la ruta del directorio de documentos.")
        sys.exit(1)

    dir_path: str = sys.argv[1]
    eliminar_palabras_vacias = False
    palabras_vacias = set()

    if len(sys.argv) > 2 and sys.argv[2] == "True" and len(sys.argv) > 3:
        eliminar_palabras_vacias = True
        archivo_palabras_vacias = sys.argv[3]
        with open(archivo_palabras_vacias, "r", encoding="utf8") as f:
            palabras_vacias = set([line.strip() for line in f])

    palabras_regex = re.compile(r'\w+')
    abreviaturas_regex = re.compile(r'\b[A-Z][a-z]*\.')
    correo_regex = re.compile(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b')
    url_regex = re.compile(r'https?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|%[0-9a-fA-F][0-9a-fA-F])+')
    num_regex = re.compile(r'\b\d+(?:\.\d+)?(?:\s+\w+)?\b')
    nombres_propios_regex = re.compile(r'\b[A-Z][a-z]+(?:\s+[A-Z][a-z]+)+\b')

    # Llamamos a la funcion tokenizar con cada una de las regex solicitadas por el tp
    tokenizar(palabras_regex, "palabras", dir_path, eliminar_palabras_vacias, palabras_vacias)
    tokenizar(correo_regex, "correo", dir_path, eliminar_palabras_vacias, palabras_vacias)
    tokenizar(url_regex, "url", dir_path, eliminar_palabras_vacias, palabras_vacias)
    tokenizar(num_regex, "num", dir_path, eliminar_palabras_vacias, palabras_vacias)
    tokenizar(nombres_propios_regex, "nombres_propios", dir_path, eliminar_palabras_vacias, palabras_vacias)
    tokenizar(abreviaturas_regex, "abreviaturas", dir_path, eliminar_palabras_vacias, palabras_vacias)


def tokenizar(regex, nombre_indice, dir_path, eliminar_palabras_vacias, palabras_vacias):
    # longitud minima y maxima para los terminos
    long_min = 1
    long_max = 100

    # diccionario para almacenar los terminos y sus estadisticas
    terminos = defaultdict(lambda: [0, set()])
    # defaultdict es un tipo de diccionario que automáticamente crea un valor por defecto en caso de que la clave
    # dada no exista. el primer elemento de la lista es la freceuncia y el segundo es un conjunto con los nombres de
    # los docs donde aparece

    # def de vaiables para estadisticas
    documentos_procesados = 0
    tokens_extraidos = 0
    terminos_extraidos = 0
    largo_terminos_total = 0
    tokens_documento_mas_corto = float("inf")
    terminos_documento_mas_corto = float("inf")
    tokens_documento_mas_largo = 0
    terminos_documento_mas_largo = 0
    terminos_con_frecuencia_uno = 0

    for filename in os.listdir(dir_path):
        if filename.endswith(".txt"):  # validamos que sean archivos de tipo texto
            with open(os.path.join(dir_path, filename), "r", encoding="utf8") as f:
                # Leemos el contenido del archivo
                contenido = f.read()  # no se normaliza a minusculas al leer para que no afecte a las regex
                if nombre_indice == "palabras":
                    contenido = contenido.lower()
                # Tokenizamos el contenido
                tokens = re.findall(regex, contenido)
                # Eliminamos las palabras vacías si se especifica
                if eliminar_palabras_vacias:
                    tokens = [token for token in tokens if token not in palabras_vacias]
                terminos_documento = defaultdict(lambda: 0)

                for token in tokens:
                    if long_min <= len(token) <= long_max:
                        terminos[token][0] += 1
                        terminos[token][1].add(filename)
                        terminos_documento[token] += 1
                        tokens_extraidos += 1  # punto (1.b.2)

                terminos_documento_extraidos = len(terminos_documento)
                terminos_extraidos += terminos_documento_extraidos  # punto (1.b.2)
                largo_terminos_total += sum([len(termino) for termino in terminos_documento])
                if terminos_documento_extraidos > 0:
                    largo_promedio_terminos_documento = \
                        sum([len(termino) for termino in terminos_documento]) / terminos_documento_extraidos
                    if largo_promedio_terminos_documento < terminos_documento_mas_corto:
                        terminos_documento_mas_corto = largo_promedio_terminos_documento  # punto (5.1)
                        tokens_documento_mas_corto = len(tokens)  # punto (5.1)
                    if largo_promedio_terminos_documento > terminos_documento_mas_largo:
                        terminos_documento_mas_largo = largo_promedio_terminos_documento  # punto (5.1)
                        tokens_documento_mas_largo = len(tokens)  # punto (5.2)
                for termino, frecuencia in terminos_documento.items():
                    if frecuencia == 1:
                        terminos_con_frecuencia_uno += 1  # punto (6)
            documentos_procesados += 1  # punto (1)

    # calcular estadisticas generales
    try:
        promedio_tokens_documento = tokens_extraidos / documentos_procesados  # punto (3.1)
        promedio_terminos_documento = terminos_extraidos / documentos_procesados  # punto (3.2)
        largo_promedio_termino = largo_terminos_total / terminos_extraidos  # punto (4)
    except ZeroDivisionError:
        print("se intento dividir por cero en el calculo de las estadisticas")
        print(documentos_procesados)
        print(terminos_extraidos)
        print(nombre_indice)
        return 0

    # Ordenamos los términos por frecuencia
    terminos_ordenados = sorted(terminos.items(), key=lambda x: x[1][0], reverse=True)

    # Escribimos el archivo de términos
    with open(f'terminos_{nombre_indice}.txt', "w") as f:
        for termino, datos in terminos_ordenados:
            cf = datos[0]
            df = len(datos[1])
            f.write(f"{termino} {cf} {df}\n")

    # Escribimos el archivo de estadisticas
    with open(f'estadisticas_{nombre_indice}.txt', "w") as f:
        f.write(f"{documentos_procesados}\n")
        f.write(f"{tokens_extraidos} {terminos_extraidos}\n")
        f.write(f"{promedio_tokens_documento:.2f} {promedio_terminos_documento:.2f}\n")
        f.write(f"{largo_promedio_termino:.2f}\n")
        f.write(f"{tokens_documento_mas_corto} {terminos_documento_mas_corto} {tokens_documento_mas_largo} "
                f"{terminos_documento_mas_largo}\n")
        f.write(f"{terminos_con_frecuencia_uno}\n")

    # Escribimos los términos más frecuentes
    with open(f'frecuencias_{nombre_indice}.txt', 'w') as f:
        f.write('Terminos mas frecuentes:\n')
        for termino, datos in terminos_ordenados[:10]:
            cf = datos[0]
            f.write(f'{termino} {cf}\n')

        # Escribir los términos menos frecuentes
        f.write('\nTerminos menos frecuentes:\n')
        for termino, datos in terminos_ordenados[-10:]:
            cf = datos[0]
            f.write(f'{termino} {cf}\n')


if __name__ == '__main__':
    main()

# Regexs para el punto 2

############ABREVIATURAS#######################################
# \b indica que la búsqueda comenzará en un límite de palabra
# (para asegurarnos de que no coincida con una parte de una palabra más larga).
# la primera parte es para para las abreviaturas que consisten en una letra mayúscula seguida de cero o
# más letras minúsculas, seguida de un punto.
# Esta parte cubre abreviaturas como "Dr.", "Sr.", "Prof.", etc.
# la segunda parte es para abreviatura con 2 o mas palabras. Por ejemplo, "U.S.A.", "Ph.D.", etc.
# La tercera corresponde a las abreviaturas que comienzan con una letra mayúscula seguida de una o más
# letras minúsculas, seguida de un punto. La palabra debe estar al comienzo de una oración o después de
# un signo de puntuación. Por ejemplo, "Ltd.", "Inc.", etc.
# ?: significa que no es un non-capturing group, lo que signifca que significa que el grupo no es
# capturado de forma separada para ser referenciado luego
###############CORREO#############################################
# [A-Za-z0-9._%+-]+ coincide con uno o más caracteres alfanuméricos, puntos, guiones bajos, porcentajes
# y signos más o menos, que corresponden a la parte del nombre de usuario del correo electrónico.
# seguido de un @
# [A-Za-z0-9.-]+ coincide con uno o más caracteres alfanuméricos, puntos y guiones,
# que corresponden a la parte del dominio del correo electrónico.
# \., coincide con un punto (el escape con \ se usa porque el punto tiene un significado especial en
# expresiones regulares).
# [A-Z|a-z]{2,} coincide con dos o más letras mayúsculas o minúsculas, que corresponden a la extensión
# del correo electrónico.
##############################URLS##################################

# http o https coincide con el protocolo de la URL.
#:// coincide con los caracteres :// que separan el protocolo del resto de la URL.
# (?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+ coincide con uno o más caracteres
# que pueden ser letras (mayúsculas o minúsculas), números o algunos caracteres especiales
# que se permiten en una URL
# La construcción (?: ... ) se usa para agrupar varias opciones en una sola expresión regular.

# r' para que trate a la cadena como "raw" y en lugar de tner que poner "\\d" poner "\d"
