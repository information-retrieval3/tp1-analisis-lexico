import os
import string
import sys

import numpy as np
from unidecode import unidecode


def main():
    if len(sys.argv) < 2:
        print("Error: No se ha especificado las rutas de los directorios de documentos de entrenamiento y de test")
        sys.exit(1)
    training_dir_path: str = sys.argv[1]
    {}
    languages_files = os.listdir(training_dir_path)
    frequency_matrices = {}
    np.zeros((26, 26))
    training_dir_path: str = sys.argv[1]
    test_file_path: str = sys.argv[2]
    output_list = []

    # Conjunto de entrenamiento
    for filename in languages_files:
        lang = filename  # usamos el filename que especifica el lang
        with open(os.path.join(training_dir_path, filename), "r", encoding="iso-8859-1") as f:
            # Leemos el contenido del archivo
            contenido = f.read()
            # Creamos la matriz de características para cada lenguaje
            frequency_matrices[lang] = generate_frequency_matrix(contenido)
    # Leemos el archivo de prueba y calcular su frecuencia de las letras
    with open(test_file_path, "r", encoding="iso-8859-1") as f:
        line_count = 0
        for line in f:
            line_count += 1
            # Creamos una matriz de características del texto de prueba
            test_features = generate_frequency_matrix(line)

            # Para cada lenguaje calcuamos correlaciones y buscamos la relacion maxima con la matrix del texto de
            # prueba
            max_index = -1
            for lang in languages_files:
                correlations = np.corrcoef(frequency_matrices[lang], test_features)
                pearson_coefficient = correlations[0, 1]
                if pearson_coefficient > max_index:
                    max_index = pearson_coefficient
                    predicted_language = lang
            # print(predicted_language)

            # Asignamos el lenguaje con la mayor correlacion
            output_list.append((line_count, predicted_language))


    # Escribimos la lsita con los resultados
    with open('5b_lang_solution.txt', 'w') as f:
        for line_count, language in output_list:
            f.write(f"{line_count} {language}\n")

#para calcular la precision:

    # Leer resultados reales
    resultados_reales = []
    with open('languageIdentificationData/solution', 'r') as r_reales:
        for line in r_reales:
            numero_linea, idioma_real = line.strip().split()
            resultados_reales.append((int(numero_linea), idioma_real))

    total = len(resultados_reales)
    correctas = sum(1 for predicho, real in zip(output_list, resultados_reales) if predicho == real)
    precision = correctas / total

    print("la precision del modelo utilizado es del ", precision)

def generate_frequency_matrix(contenido):
    # Pasamos a minuculas
    contenido = contenido.lower()
    # Eliminamos signos de puntuación
    contenido = contenido.translate(str.maketrans('', '', string.punctuation))
    # Tokenizar en palabras
    contenido = contenido.split()
    # sacamos los acentos raros
    contenido = [unidecode(word) for word in contenido]

    # diccionario de las frecuencias de cada letra minuscula:
    frequencies = {chr(i): 0 for i in range(97, 123)}
    # lista de cada combinacion de par de letras:
    pairs = [chr(i) + chr(j) for i in range(97, 123) for j in range(97, 123)]
    for pair in pairs:
        frequencies[pair] = 0
    for word in contenido:
        for i in range(len(word) - 1):
            pair = word[i:i + 2]
            if pair.isalpha() and len(pair) == 2:
                frequencies[pair.lower()] += 1
    return list(frequencies.values())


if __name__ == '__main__':
    main()
