import math
import os
import re
import sys

import numpy as np
from langdetect import detect

languages = ["English", "French", "Italian"]
training_dir_path: str = sys.argv[1]
test_file_path: str = sys.argv[2]
output_list = []
prob_langs = {}


def main():
    import re

    # Ejecucion de la prueba
    with open(test_file_path, "r", encoding="iso-8859-1") as f:
        line_count = 0
        for line in f:
            line_count += 1
            # Tokenizamos el contenido
            language = detect(line)
            #lo guardo con el mismo formato para poder comparar
            if language == 'it': language = 'Italian'
            if language == 'en': language = 'English'
            if language == 'fr': language = 'French'
            output_list.append((line_count, language))


    # Escribimos la lsita con los resultados
    with open('5c_lang_solution.txt', 'w') as f:
        for line_count, language in output_list:
            f.write(f"{line_count} {language}\n")

#para calcular la precision:

    # Leer resultados reales
    resultados_reales = []
    with open('languageIdentificationData/solution', 'r') as r_reales:
        for line in r_reales:
            numero_linea, idioma_real = line.strip().split()
            resultados_reales.append((int(numero_linea), idioma_real))

    total = len(resultados_reales)
    correctas = sum(1 for predicho, real in zip(output_list, resultados_reales) if predicho == real)
    precision = correctas / total

    print("la precision del modelo utilizado es del ", precision)

if __name__ == '__main__':
    main()
