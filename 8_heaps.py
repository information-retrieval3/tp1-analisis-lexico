import nltk
from nltk.tokenize import word_tokenize
from collections import Counter

def contar_terminos(archivo_entrada):
    # Contador para almacenar el total de términos procesados
    total_terminos = 0
    # Diccionario para almacenar los términos únicos
    terminos_unicos = set()

    with open(archivo_entrada, 'r') as file:
        with open("heaps.txt", 'w') as output_file:
            for linea in file:
                # Tokenizar la línea palabra por palabra
                palabras = word_tokenize(linea.lower())
                for palabra in palabras:
                    # Incrementar el contador de términos procesados
                    total_terminos += 1
                    # Agregar la palabra al conjunto de términos únicos
                    terminos_unicos.add(palabra)
                    # Escribir el par (#términos totales procesados #términos únicos) en el archivo de salida
                    output_file.write(f"{total_terminos} {len(terminos_unicos)}\n")

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print("Uso: python script.py archivo_entrada")
        sys.exit(1)
    archivo_entrada = sys.argv[1]
    contar_terminos(archivo_entrada)
