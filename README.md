Instrucciones:

1. Las consignas están en consignas.pdf
2. Comandos para ejecutar los ejercicios, por ejemplo:
- python3 1a_analisis_lexico.py RI-tknz-data true palabras_vacias.txt
- python3 2_analisis_lexico.py RI-tknz-data True palabras_vacias.txt
- python3 3_analisis_lexico.py RI-tknz-data True palabras_vacias.txt
- python3 4_analisis_lexico.py RI-tknz-data
- python3 5a_analisis_lexico.py languageIdentificationData/training languageIdentificationData/test
- python3 8_heaps.py RI-tknz-data/academiacisco.unnoba.edu.ar.txt

El resto de programas están en la carpeta /notebook.
Usar jupyter para ejecutarlos (pip install jupyter, jupyter notebook)